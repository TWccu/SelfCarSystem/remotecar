class CarState:
    def __init__(self):
        self.state = 0

    def get_state(self):
        return self.state

    def set_state(self, state):
        self.state = state
