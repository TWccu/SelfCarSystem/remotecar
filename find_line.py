from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np
import math


class SelfCarEyes:

    def __init__(self, height=480, weight=640, car_state=None, ccuPiMotor=None):
        self.ccuPiMotor = ccuPiMotor
        self.car_state = car_state
        self.theta = 0
        self.minLineLength = 100
        self.maxLineGap = 10
        self.height = height
        self.weight = weight
        self.hasGate = 0
        self.counter = 0
        self.camera = PiCamera()
        self.camera.resolution = (640, 480)
        self.camera.framerate = 30
        self.rawCapture = PiRGBArray(self.camera, size=(weight, height))
        time.sleep(0.1)

    def init_camera(self):
        self.rawCapture.truncate(0)
        time.sleep(0.1)

    def follow_line(self):
        for frame in self.camera.capture_continuous(self.rawCapture, format="bgr", use_video_port=True):

            if self.car_state.get_state() == 0:
                break

            image = frame.array[280:self.height, 10:self.weight - 10]

            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            blurred = cv2.GaussianBlur(gray, (5, 5), 0)
            edged = cv2.Canny(blurred, 85, 85)

            lines = cv2.HoughLinesP(edged, 1, np.pi / 180, 10, self.minLineLength, self.maxLineGap)

            if lines is not None:
                for x in range(0, len(lines)):
                    for x1, y1, x2, y2 in lines[x]:
                        cv2.line(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
                        theta = theta + math.atan2((y2 - y1), (x2 - x1))

            threshold = 6
            print(theta)
            if theta > threshold:  # left
                self.ccuPiMotor.setSpeed(80, 50)
            elif theta < -threshold:  # right
                self.ccuPiMotor.setSpeed(50, 80)
            elif abs(theta) < threshold:  # straight
                self.ccuPiMotor.setSpeed(50, 50)

            theta = 0

            self.rawCapture.truncate(0)
            
    def follow_line2(self):
        for frame in self.camera.capture_continuous(self.rawCapture, format="bgr", use_video_port=True):
            camera_image = cv2.GaussianBlur(frame.array[:, :], (7,7), 0)
            camera_image = cv2.cvtColor(camera_image, cv2.COLOR_BGR2GRAY)
            
            doorImage = camera_image[:300, :]
            roadImage = camera_image[300:,:]
            roadImage = cv2.threshold(roadImage, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
            right = np.sum(roadImage[:, 440:], dtype=int)
            left = np.sum(roadImage[:, :200], dtype=int)
        ##    print("R: ",right)
        ##    print("L: ",left)
            if (right - left) > 200:
                self.ccuPiMotor.setSpeed(80, 50)
            elif (right - left) < -200:
                self.ccuPiMotor.setSpeed(50, 80)
            else:
                self.ccuPiMotor.setSpeed(50, 50)
            
            camera = cv2.GaussianBlur(frame.array[:, :], (5,5), 0)
            gateImage = camera[:350, :]
            cv2.imshow("gateImage",gateImage)
            camera = cv2.cvtColor(camera_image, cv2.COLOR_BGR2GRAY)
            self.findNum(gateImage)

            roadImage = camera_image[350:, :]
            cv2.imshow("roadImage",roadImage)
            roadImage = cv2.threshold(roadImage, 150, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

            right = np.sum(roadImage[:, 440:], dtype=int)
            left = np.sum(roadImage[:, :200], dtype=int)
        ##    print(right - left)
        ##
            if(self.hasGate < 0):
                if (right - left) > 8000:
                    self.ccuPiMotor.setSpeed(50+self.counter, 0-self.counter)
                    self.counter += 0.1
                    if self.counter > 50:
                        self.counter = 50
                elif (right - left) < -8000:
                    self.ccuPiMotor.setSpeed(0-self.counter, 50+self.counter)
                    self.counter += 0.1
                    if self.counter > 50:
                        self.counter = 50
                else:
                    self.counter = 0
                    self.ccuPiMotor.setSpeed(60, 60)
            
            cv2.imshow("Origin",frame.array[:, :])
            cv2.imshow("roadImage_Binary",roadImage)
            self.rawCapture.truncate(0)
                
    def findNum(image):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        binaryImage = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY_INV)[1]
        edged = cv2.Canny(binaryImage, 30, 150)
        cntsLevel = cv2.findContours(edged, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        cntsLevel = cntsLevel[0] if imutils.is_cv2() else cntsLevel[1]
        rect = None
        
        for c in cntsLevel:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.03*peri, True)
    ##        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 1)
            if len(approx) == 4:
                area = cv2.contourArea(approx)
                if((area > 5000) and (area < 12000)):
                    rect = approx
                    break
        self.hasGate -= 1
        if rect is not None:
            self.hasGate = 20
            ccuPiMotor.stop()
            warped = four_point_transform(gray, rect.reshape(4,2))
            self.getNum(warped)
        print(hasGate)

    def getNum(warped):
        try:
            warpedBinaryImage = cv2.threshold(warped, 150, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
            
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 5))
            warpedBinaryImage = cv2.dilate(warpedBinaryImage,kernel,iterations=1)
            
            areaBios = 4
            (x, y, numberAreaWight, numberAreaHigh) = cv2.boundingRect(warpedBinaryImage)
            warpedBinaryImage[:, :areaBios+1] = 0
            warpedBinaryImage[:areaBios+1, :] = 0
            warpedBinaryImage[warpedBinaryImage.shape[0] - areaBios:, :] = 0
            warpedBinaryImage[:, warpedBinaryImage.shape[1] - areaBios:] = 0
            halfNumberAreaHigh = numberAreaHigh*0.7
            oneThirdNumberAreaHigh = numberAreaHigh*0.3
            image, numberConts, numberHierarchy = cv2.findContours(warpedBinaryImage, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            maxNumberW = 0
            usedNumberConts = []
            for i in range(len(numberConts)):
                (ix, iy, iw, ih) = cv2.boundingRect(numberConts[i])
                if ih > oneThirdNumberAreaHigh and iw > 15 and numberHierarchy[0][i][3] == -1:
                    (x, y, w, h) = cv2.boundingRect(numberConts[i])
                    maxNumberW = max(w,maxNumberW)
                    usedNumberConts.append(numberConts[i])
                    
            thresh3 = cv2.cvtColor(warpedBinaryImage, cv2.COLOR_GRAY2BGR)
                
            for c in usedNumberConts:
                # extract the digit ROI
                (x, y, w, h) = cv2.boundingRect(c)
                if w < maxNumberW * 0.5:
                    cv2.rectangle(thresh3, (x, y), (x + w, y + h), (0, 255, 0), 1)
                    cv2.putText(thresh3, str(1), (x + 15, y + 15), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 0, 0), 1)
                    continue

                roi = warpedBinaryImage[y:y + h, x:x + w]

                # compute the width and height of each of the 7 segments
                # we are going to examine
                (roiH, roiW) = roi.shape
                (dW, dH) = (int(roiW * 0.2), int(roiH * 0.125))
                dHC = int(dH * 0.5)
                
                segments = [
                    ((2 * dW - dW // 2, 0), (3 * dW + dW // 2, dH - dH // 2)),  # top
                    ((dW // 2, dH), (dW + dW //2, 3 * dH)),  # top-left
                    ((4 * dW - dW // 2, dH), (w - (dW // 2), 3 * dH)),  # top-right
                    ((2 * dW - dW // 2, 4 * dH - dHC), (3 * dW + dW // 2, 4 * dH + dHC)),  # center
                    ((dW // 2, 5 * dH), (dW + dW //2, 7 * dH)),  # bottom-left
                    ((4 * dW - dW // 2, 5 * dH), (w - (dW // 2), 7 * dH)),  # bottom-right
                    ((2 * dW - dW // 2, 7 * dH + dHC), (3 * dW + dW // 2, 8 * dH))  # bottom
                ]
                on = [0] * len(segments)
                # loop over the segments
                for (i, ((xA, yA), (xB, yB))) in enumerate(segments):
                    # extract the segment ROI, count the total number of
                    # thresholded pixels in the segment, and then compute
                    # the area of the segment
                    segROI = roi[yA:yB, xA:xB]
                    total = cv2.countNonZero(segROI)
                    area = (xB - xA) * (yB - yA)
                    # if the total number of non-zero pixels is greater than
                    # 50% of the area, mark the segment as "on"
                    if total / float(area) > 0.5:
                        on[i] = 1

                    # lookup the digit and draw it on the image
                digit = DIGITS_LOOKUP[tuple(on)]
                cv2.rectangle(thresh3, (x, y), (x + w, y + h), (0, 255, 0), 1)
                cv2.putText(thresh3, str(digit), (x + 15 , y + 15), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 0, 0), 1)
                print(digit)
                cv2.imshow("warped", thresh3)
        except:
            return
